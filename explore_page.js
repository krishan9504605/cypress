export class ExplorePage{
    clickSearch(){
        cy.xpath("//div[contains(text(), 'All Opportunities')]").should('have.text', 'All Opportunities')
        cy.get('.global_search > input[placeholder="Search Opportunities"]').click()
        cy.get('.filter_single_select:first-of-type').should('have.text', ' All Opportunities ');
        cy.get('.filter_single_select.margin-with-border > .selected_filter').should('have.text', 'Sort By');
        cy.get('.d-flex > .j-between > .align-center > .margin-with-border > .selected_filter').should('have.text', 'Filters');
    }
}