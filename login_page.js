import { contains } from "cypress/types/jquery"

export class LoginPage{
    constructor(){
        this.email = "dummy496@d2ctest.com"
        this.password = "abcdefg"
    }

    navigate(url){
        cy.visit(url)
    }

    enterUsername(){
        cy.get('#email').type(this.email)
    }

    enterPassword(){
        cy.get('#pwd').type(this.password)
    }

    clickLogin(){
        cy.get('.submit_btn').click()
    }

    byPass(){
        const variable_key = 'c_BP'
        const variable_value = 'aldkskkfhkjgFDGHJYHGF3hjvhvhvh56vdsdf456fvB'
        cy.window().then((win) => {
            win.localStorage.setItem(variable_key, variable_value);
        })
        cy.window().then((win) => {
            win.scrollTo(0, 500); // Scrolls vertically to 500 pixels from the top
        })
        cy.xpath("//div[contains(text(),'Ok, Continue')]").click()
    }

}
